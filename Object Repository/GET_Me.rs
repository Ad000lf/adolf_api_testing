<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GET_Me</name>
   <tag></tag>
   <elementGuidId>dca57b35-b77c-4139-b511-df94917219fe</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zYW5kYm94LmFwaS5vZXhwcmVzcy5jby5pZFwvYXV0aFwvbWVtYmVyXC9sb2dpbiIsImlhdCI6MTY5NDIzODQ2NCwiZXhwIjoxNzU2NDQ2NDY0LCJuYmYiOjE2OTQyMzg0NjQsImp0aSI6ImZOVU9yWU1tRUl4alBTcWciLCJzdWIiOiI2NGY1YmU3ZmI0NGI0NzNlYTI3NjVkMTIiLCJwcnYiOiI4NjY1YWU5Nzc1Y2YyNmY2YjhlNDk2Zjg2ZmE1MzZkNjhkZDcxODE4IiwidHlwZSI6Im1lbWJlciJ9.uHr6ia_lZh6ooKnY6u2dqjBARQb5oyULvHW8NhQj5RU</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-api-key</name>
      <type>Main</type>
      <value>A298535C-B6AE-4EE4-B9B2-A07C996E76A7</value>
      <webElementGuid>b3469259-d2bb-4040-91af-e5cdce0e9e4d</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zYW5kYm94LmFwaS5vZXhwcmVzcy5jby5pZFwvYXV0aFwvbWVtYmVyXC9sb2dpbiIsImlhdCI6MTY5NDIzODQ2NCwiZXhwIjoxNzU2NDQ2NDY0LCJuYmYiOjE2OTQyMzg0NjQsImp0aSI6ImZOVU9yWU1tRUl4alBTcWciLCJzdWIiOiI2NGY1YmU3ZmI0NGI0NzNlYTI3NjVkMTIiLCJwcnYiOiI4NjY1YWU5Nzc1Y2YyNmY2YjhlNDk2Zjg2ZmE1MzZkNjhkZDcxODE4IiwidHlwZSI6Im1lbWJlciJ9.uHr6ia_lZh6ooKnY6u2dqjBARQb5oyULvHW8NhQj5RU</value>
      <webElementGuid>5c5ef440-51b0-453b-b44f-8093bf662ecf</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.14.alpha</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://sandbox.api.oexpress.co.id/auth/member/me</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
