<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>PUT_Verification Update Email</name>
   <tag></tag>
   <elementGuidId>2d488d02-8000-419f-948a-02adde329d82</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zYW5kYm94LmFwaS5vZXhwcmVzcy5jby5pZFwvYXV0aFwvbWVtYmVyXC9sb2dpbiIsImlhdCI6MTY5NDIzOTY3MSwiZXhwIjoxNzU2NDQ3NjcxLCJuYmYiOjE2OTQyMzk2NzEsImp0aSI6Ilo4MHNMZUtnV2ZrS1RZVHQiLCJzdWIiOiI2NGY5ZDZjOGM5Y2FkMzdmODc3MzE2MDIiLCJwcnYiOiI4NjY1YWU5Nzc1Y2YyNmY2YjhlNDk2Zjg2ZmE1MzZkNjhkZDcxODE4IiwidHlwZSI6Im1lbWJlciJ9.euLpaqTDHBat08EaNmVjGKSzT-cM5Pyp59VvlXRZSnM</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;otp\&quot;: \&quot;121581\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>01fb434d-cade-4e3b-96d2-8647459f6640</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-api-key</name>
      <type>Main</type>
      <value>A298535C-B6AE-4EE4-B9B2-A07C996E76A7</value>
      <webElementGuid>4140a224-275d-4ad9-8974-711757794f4a</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zYW5kYm94LmFwaS5vZXhwcmVzcy5jby5pZFwvYXV0aFwvbWVtYmVyXC9sb2dpbiIsImlhdCI6MTY5NDIzOTY3MSwiZXhwIjoxNzU2NDQ3NjcxLCJuYmYiOjE2OTQyMzk2NzEsImp0aSI6Ilo4MHNMZUtnV2ZrS1RZVHQiLCJzdWIiOiI2NGY5ZDZjOGM5Y2FkMzdmODc3MzE2MDIiLCJwcnYiOiI4NjY1YWU5Nzc1Y2YyNmY2YjhlNDk2Zjg2ZmE1MzZkNjhkZDcxODE4IiwidHlwZSI6Im1lbWJlciJ9.euLpaqTDHBat08EaNmVjGKSzT-cM5Pyp59VvlXRZSnM</value>
      <webElementGuid>c6b7be63-22c0-4041-86b6-932d64c6fe08</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.14.alpha</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>https://sandbox.api.oexpress.co.id/settings/verify-otp-change-email</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
