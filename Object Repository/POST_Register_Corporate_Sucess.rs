<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>POST_Register_Corporate_Sucess</name>
   <tag></tag>
   <elementGuidId>8a61ca98-984c-4b08-9c98-3409da9e3059</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;name\&quot;: \&quot;PT.GudangIndoneisa\&quot;,\n\t\&quot;email\&quot;: \&quot;hc@gudangindo.co.id\&quot;,\n\t\&quot;password\&quot;: \&quot;Semangat12345!\&quot;,\n\t\&quot;phone\&quot;: \&quot;0627898989\&quot;,\n\t\&quot;phone_alt\&quot;: \&quot;06278989900\&quot;,\n\t\&quot;address\&quot;: \&quot;Kiara Condong\&quot;,\n\t\&quot;branch_id\&quot;: \&quot;619ca842f936eb2a7c1422b7\&quot;,\n    \&quot;reff_code\&quot;: \&quot;H7PTW1\&quot;,\n    \&quot;member_category\&quot;: \&quot;corporate\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>64e69603-9ca1-413f-81ed-f4034efd0de4</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-api-key</name>
      <type>Main</type>
      <value>A298535C-B6AE-4EE4-B9B2-A07C996E76A7</value>
      <webElementGuid>5155bc80-ded1-44c1-9d65-b7cf9ee3bad7</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.14.alpha</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://sandbox.api.oexpress.co.id/auth/member/register</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
