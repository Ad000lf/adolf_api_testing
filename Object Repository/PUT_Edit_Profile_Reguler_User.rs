<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>PUT_Edit_Profile_Reguler_User</name>
   <tag></tag>
   <elementGuidId>ce3486ab-5565-4657-a303-75a2058d9851</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zYW5kYm94LmFwaS5vZXhwcmVzcy5jby5pZFwvYXV0aFwvbWVtYmVyXC9sb2dpbiIsImlhdCI6MTY5NDIzODQ2NCwiZXhwIjoxNzU2NDQ2NDY0LCJuYmYiOjE2OTQyMzg0NjQsImp0aSI6ImZOVU9yWU1tRUl4alBTcWciLCJzdWIiOiI2NGY1YmU3ZmI0NGI0NzNlYTI3NjVkMTIiLCJwcnYiOiI4NjY1YWU5Nzc1Y2YyNmY2YjhlNDk2Zjg2ZmE1MzZkNjhkZDcxODE4IiwidHlwZSI6Im1lbWJlciJ9.uHr6ia_lZh6ooKnY6u2dqjBARQb5oyULvHW8NhQj5RU</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;name\&quot;: \&quot;Acil\&quot;,\n    \&quot;phone\&quot;: \&quot;099989283798\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>2d71e7bc-7b2a-48df-bf96-7ee8617732c3</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-api-key</name>
      <type>Main</type>
      <value>A298535C-B6AE-4EE4-B9B2-A07C996E76A7</value>
      <webElementGuid>27e29bc5-2373-4869-b3f3-847e3ae5bb61</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zYW5kYm94LmFwaS5vZXhwcmVzcy5jby5pZFwvYXV0aFwvbWVtYmVyXC9sb2dpbiIsImlhdCI6MTY5NDIzODQ2NCwiZXhwIjoxNzU2NDQ2NDY0LCJuYmYiOjE2OTQyMzg0NjQsImp0aSI6ImZOVU9yWU1tRUl4alBTcWciLCJzdWIiOiI2NGY1YmU3ZmI0NGI0NzNlYTI3NjVkMTIiLCJwcnYiOiI4NjY1YWU5Nzc1Y2YyNmY2YjhlNDk2Zjg2ZmE1MzZkNjhkZDcxODE4IiwidHlwZSI6Im1lbWJlciJ9.uHr6ia_lZh6ooKnY6u2dqjBARQb5oyULvHW8NhQj5RU</value>
      <webElementGuid>615a13c3-c01b-40ff-b08c-0a6e43f56f76</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.14.alpha</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>https://sandbox.api.oexpress.co.id/settings/profile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
